var http = require('http');
var fs = require('fs');
var path = require('path');

const PORT = 8080;
const HOST = '0.0.0.0';

http.createServer(function (request, response) {
    console.log('request ', request.url);

    response.statusCode = 200;
    response.setHeader('Content-Type', 'text/plain');
    response.end('Hello 123!!!');
}).listen(PORT, HOST);

console.log(`Running on http://${HOST}:${PORT}`);